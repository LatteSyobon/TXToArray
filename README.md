# TXToArray : Creating an array from a text file
Library to create string type arrays from text files (or readable files)

# Demo
Sample projects using TXTA
[TXTalker](https://github.com/LatteSyobon/TXTalker)
 
# Usage
 
```C++
#include <stdio.h>
#include <string>

#include "txta.h"

int main() {
	std::string index[100];
	std::string path = "./Sample.txt"; // Text file to be read
	int array_num = 0;
	array_num = txttorawarray_str(path, index);
	for (int count = 0; count < array_num; count = count + 1)
		printf("%s\n", index[count].c_str());
}

```


Sample.txt
```TXT
ABC
DEF
GHI
JKL
MNO
PQR
STU
VWX
YZA
```
 
# Note
 
At this stage, only strings are supported.  
Also, the text file to be read must be in ANSI format or it will be garbled!
 
# Author
 
* Lattex
 
# License
"TXToArray" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).
