#pragma once

#include <string>
#include <fstream>

int txttorawarray_str(std::string file_path, std::string *index);
int txttorawarray_int(std::string file_path, int* index);

int getlines(std::string file_path);