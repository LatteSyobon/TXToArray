﻿// txta.cpp : スタティック ライブラリ用の関数を定義します。
//

#include "pch.h"
#include "framework.h"

#include <fstream>
#include <iostream>
#include <string>

#include "txta.h"

int txttorawarray_str(std::string file_path, std::string* index)
{

    std::string str;
    std::fstream file(file_path);

    int lines = getlines(file_path);
    lines++;

    int array_num = 0;

    if (file.fail()) {
        std::cerr << "[txta|Error]Failed to open file." << std::endl;
    }
    for (int i = 0; i < lines; i++) {
        getline(file, str);
        index[i] = str;
        if (file.eof()) {
            array_num = i;
            return array_num;
        }
    }
    return array_num;
}

int txttorawarray_int(std::string file_path, int* index)
{
    std::string str;
    std::fstream file(file_path);

    int lines = getlines(file_path);
    lines++;

    int array_num = 0;

    if (file.fail()) {
        std::cerr << "[txta|Error]Failed to open file." << std::endl;
    }
    for (int i = 0; i < lines; i++) {
        getline(file, str);
        index[i] = std::stoi(str);
        if (file.eof()) {
            array_num = i;
            return array_num;
        }
    }
    return array_num;
}

int getlines(std::string file_path)
{
    long i = 0;
    std::ifstream ifs(file_path);

    if (ifs)
    {
        std::string line;

        while (true)
        {
            getline(ifs, line);
            i++;
            if (ifs.eof())
                break;
        }
    }
    return i;
}